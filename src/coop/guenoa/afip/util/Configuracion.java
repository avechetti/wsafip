/**
 * 
 */
package coop.guenoa.afip.util;

import java.io.FileInputStream;
import java.util.Properties;

/**
 * @author avechetti
 *
 */
public class Configuracion {

	private Boolean HOMOLOCION = true;
	private Boolean DEBUG = true;

	private String DSTDN = "cn=wsaahomo,o=afip,c=ar,serialNumber=CUIT 33693450239";
	private String KEYSTORE = null;
	private String KEYSTORE_USER = null;
	private String KEYSTORE_PASS = null;

	private String HTTP_PROXY = null;
	private String HTTP_PROXY_PORT = null;
	private String HTTP_PROXY_USER = null;
	private String HTTP_PROXY_PASS = null;
	private Integer TICKETTIME = 60 * 60;

	private String CUIT = null;

	public Boolean getHOMOLOCION() {
		return HOMOLOCION;
	}

	public void setHOMOLOCION(Boolean hOMOLOCION) {
		HOMOLOCION = hOMOLOCION;
	}

	public Boolean getDEBUG() {
		return DEBUG;
	}

	public void setDEBUG(Boolean dEBUG) {
		DEBUG = dEBUG;
	}

	public String getKEYSTORE() {
		return KEYSTORE;
	}

	public void setKEYSTORE(String kEYSTORE) {
		KEYSTORE = kEYSTORE;
	}

	public String getKEYSTORE_USER() {
		return KEYSTORE_USER;
	}

	public void setKEYSTORE_USER(String kEYSTORE_USER) {
		KEYSTORE_USER = kEYSTORE_USER;
	}

	public String getKEYSTORE_PASS() {
		return KEYSTORE_PASS;
	}

	public void setKEYSTORE_PASS(String kEYSTORE_PASS) {
		KEYSTORE_PASS = kEYSTORE_PASS;
	}

	public String getHTTP_PROXY() {
		return HTTP_PROXY;
	}

	public void setHTTP_PROXY(String hTTP_PROXY) {
		HTTP_PROXY = hTTP_PROXY;
	}

	public String getHTTP_PROXY_PORT() {
		return HTTP_PROXY_PORT;
	}

	public void setHTTP_PROXY_PORT(String hTTP_PROXY_PORT) {
		HTTP_PROXY_PORT = hTTP_PROXY_PORT;
	}

	public String getHTTP_PROXY_USER() {
		return HTTP_PROXY_USER;
	}

	public void setHTTP_PROXY_USER(String hTTP_PROXY_USER) {
		HTTP_PROXY_USER = hTTP_PROXY_USER;
	}

	public String getHTTP_PROXY_PASS() {
		return HTTP_PROXY_PASS;
	}

	public void setHTTP_PROXY_PASS(String hTTP_PROXY_PASS) {
		HTTP_PROXY_PASS = hTTP_PROXY_PASS;
	}

	public Integer getTICKETTIME() {
		return TICKETTIME;
	}

	public void setTICKETTIME(Integer tICKETTIME) {
		TICKETTIME = tICKETTIME;
	}

	public String getCUIT() {
		return CUIT;
	}

	public void setCUIT(String cUIT) {
		CUIT = cUIT;
	}

	public void readProperty() {
		readProperty("./wsafip.properties");
	}

	
	public String getDSTDN() {
		return DSTDN;
	}

	public void setDSTDN(String dSTDN) {
		DSTDN = dSTDN;
	}

	public void readProperty(String filename) {
		Properties config = new Properties();

		try {
			config.load(new FileInputStream(filename));
		} catch (Exception e) {
			e.printStackTrace();
		}

		DSTDN = config.getProperty("dstdn", "cn=wsaahomo,o=afip,c=ar,serialNumber=CUIT 33693450239");

		KEYSTORE = config.getProperty("keystore", "certificado.p12");
		KEYSTORE_USER = config.getProperty("keystore-signer", "");
		KEYSTORE_PASS = config.getProperty("keystore-password", "");

		HTTP_PROXY = config.getProperty("http_proxy", "");
		HTTP_PROXY_PORT = config.getProperty("http_proxy_port", "");
		HTTP_PROXY_USER = config.getProperty("http_proxy_user", "");
		HTTP_PROXY_PASS = config.getProperty("http_proxy_password", "");

		CUIT = config.getProperty("cuit");

		/*
		 * // Set proxy system vars System.setProperty("http.proxyHost",
		 * config.getProperty("http_proxy", "")); System.setProperty("http.proxyPort",
		 * config.getProperty("http_proxy_port", ""));
		 * System.setProperty("http.proxyUser", config.getProperty("http_proxy_user",
		 * "")); System.setProperty("http.proxyPassword",
		 * config.getProperty("http_proxy_password", ""));
		 */
		// Set the keystore used by SSL
		/*
		 * System.setProperty("javax.net.ssl.trustStore",
		 * config.getProperty("trustStore", ""));
		 * System.setProperty("javax.net.ssl.trustStorePassword",
		 * config.getProperty("trustStore_password", ""));
		 */
	}
}
