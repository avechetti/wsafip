package coop.guenoa.afip.wsfev1;

public class WsFe_v1 implements coop.guenoa.afip.wsfev1.IServiceWsfeV1 {
	private String _endpoint = null;
	private IServiceWsfeV1 serviceSoap = null;
	private Boolean homologacion = true;

	public WsFe_v1(Boolean homologacion) {
		this.homologacion = homologacion;
		// _initServiceSoapProxy();
	}

	/*
	 * public ServiceSoap(String endpoint) { _endpoint = endpoint;
	 * _initServiceSoapProxy(); }
	 */

	private void _initServiceSoapProxy() {
		try {
			serviceSoap = (new coop.guenoa.afip.wsfev1.ServiceLocator(homologacion)).getServiceSoap();
			if (serviceSoap != null) {
				if (_endpoint != null)
					((javax.xml.rpc.Stub) serviceSoap)._setProperty("javax.xml.rpc.service.endpoint.address",
							_endpoint);
				else
					_endpoint = (String) ((javax.xml.rpc.Stub) serviceSoap)
							._getProperty("javax.xml.rpc.service.endpoint.address");
			}

		} catch (javax.xml.rpc.ServiceException serviceException) {
		}
	}

	public String getEndpoint() {
		return _endpoint;
	}

	public void setEndpoint(String endpoint) {
		_endpoint = endpoint;
		if (serviceSoap != null)
			((javax.xml.rpc.Stub) serviceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);

	}

	public coop.guenoa.afip.wsfev1.IServiceWsfeV1 getServiceSoap() {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap;
	}

	@Override
	public coop.guenoa.afip.wsfev1.FECAEResponse FECAESolicitar(coop.guenoa.afip.wsfev1.FEAuthRequest auth, coop.guenoa.afip.wsfev1.FECAERequest feCAEReq)
			throws java.rmi.RemoteException, WsFev1Execepcion {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FECAESolicitar(auth, feCAEReq);
	}

	@Override
	public coop.guenoa.afip.wsfev1.FERegXReqResponse FECompTotXRequest(coop.guenoa.afip.wsfev1.FEAuthRequest auth) throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FECompTotXRequest(auth);
	}

	@Override
	public coop.guenoa.afip.wsfev1.DummyResponse FEDummy() throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FEDummy();
	}

	@Override
	public coop.guenoa.afip.wsfev1.FERecuperaLastCbteResponse FECompUltimoAutorizado(coop.guenoa.afip.wsfev1.FEAuthRequest auth, int ptoVta, int cbteTipo)
			throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FECompUltimoAutorizado(auth, ptoVta, cbteTipo);
	}

	@Override
	public coop.guenoa.afip.wsfev1.FECompConsultaResponse FECompConsultar(coop.guenoa.afip.wsfev1.FEAuthRequest auth,
			coop.guenoa.afip.wsfev1.FECompConsultaReq feCompConsReq) throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FECompConsultar(auth, feCompConsReq);
	}

	@Override
	public coop.guenoa.afip.wsfev1.FECAEAResponse FECAEARegInformativo(coop.guenoa.afip.wsfev1.FEAuthRequest auth, coop.guenoa.afip.wsfev1.FECAEARequest feCAEARegInfReq)
			throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FECAEARegInformativo(auth, feCAEARegInfReq);
	}

	@Override
	public coop.guenoa.afip.wsfev1.FECAEAGetResponse FECAEASolicitar(coop.guenoa.afip.wsfev1.FEAuthRequest auth, int periodo, short orden)
			throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FECAEASolicitar(auth, periodo, orden);
	}

	@Override
	public coop.guenoa.afip.wsfev1.FECAEASinMovConsResponse FECAEASinMovimientoConsultar(coop.guenoa.afip.wsfev1.FEAuthRequest auth,
			java.lang.String CAEA, int ptoVta) throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FECAEASinMovimientoConsultar(auth, CAEA, ptoVta);
	}

	@Override
	public coop.guenoa.afip.wsfev1.FECAEASinMovResponse FECAEASinMovimientoInformar(coop.guenoa.afip.wsfev1.FEAuthRequest auth, int ptoVta,
			java.lang.String CAEA) throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FECAEASinMovimientoInformar(auth, ptoVta, CAEA);
	}

	@Override
	public coop.guenoa.afip.wsfev1.FECAEAGetResponse FECAEAConsultar(coop.guenoa.afip.wsfev1.FEAuthRequest auth, int periodo, short orden)
			throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FECAEAConsultar(auth, periodo, orden);
	}

	@Override
	public coop.guenoa.afip.wsfev1.FECotizacionResponse FEParamGetCotizacion(coop.guenoa.afip.wsfev1.FEAuthRequest auth, java.lang.String monId)
			throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FEParamGetCotizacion(auth, monId);
	}

	@Override
	public coop.guenoa.afip.wsfev1.FETributoResponse FEParamGetTiposTributos(coop.guenoa.afip.wsfev1.FEAuthRequest auth) throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FEParamGetTiposTributos(auth);
	}

	@Override
	public coop.guenoa.afip.wsfev1.MonedaResponse FEParamGetTiposMonedas(coop.guenoa.afip.wsfev1.FEAuthRequest auth) throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FEParamGetTiposMonedas(auth);
	}

	@Override
	public coop.guenoa.afip.wsfev1.IvaTipoResponse FEParamGetTiposIva(coop.guenoa.afip.wsfev1.FEAuthRequest auth) throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FEParamGetTiposIva(auth);
	}

	@Override
	public coop.guenoa.afip.wsfev1.OpcionalTipoResponse FEParamGetTiposOpcional(coop.guenoa.afip.wsfev1.FEAuthRequest auth)
			throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FEParamGetTiposOpcional(auth);
	}

	@Override
	public coop.guenoa.afip.wsfev1.ConceptoTipoResponse FEParamGetTiposConcepto(coop.guenoa.afip.wsfev1.FEAuthRequest auth)
			throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FEParamGetTiposConcepto(auth);
	}

	@Override
	public coop.guenoa.afip.wsfev1.FEPtoVentaResponse FEParamGetPtosVenta(coop.guenoa.afip.wsfev1.FEAuthRequest auth) throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FEParamGetPtosVenta(auth);
	}

	@Override
	public coop.guenoa.afip.wsfev1.CbteTipoResponse FEParamGetTiposCbte(coop.guenoa.afip.wsfev1.FEAuthRequest auth) throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FEParamGetTiposCbte(auth);
	}

	@Override
	public coop.guenoa.afip.wsfev1.DocTipoResponse FEParamGetTiposDoc(coop.guenoa.afip.wsfev1.FEAuthRequest auth) throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FEParamGetTiposDoc(auth);
	}

	@Override
	public coop.guenoa.afip.wsfev1.FEPaisResponse FEParamGetTiposPaises(coop.guenoa.afip.wsfev1.FEAuthRequest auth) throws java.rmi.RemoteException {
		if (serviceSoap == null)
			_initServiceSoapProxy();
		return serviceSoap.FEParamGetTiposPaises(auth);
	}

}