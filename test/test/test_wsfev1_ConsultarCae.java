package test;

import java.rmi.RemoteException;

import coop.guenoa.afip.util.Configuracion;
import coop.guenoa.afip.util.Debug;
import coop.guenoa.afip.wsaa.TicketLogin;
import coop.guenoa.afip.wsaa.Wsaa;
import coop.guenoa.afip.wsaa.WsaaException;
import coop.guenoa.afip.wsfev1.DummyResponse;
import coop.guenoa.afip.wsfev1.Err;
import coop.guenoa.afip.wsfev1.Evt;
import coop.guenoa.afip.wsfev1.FEAuthRequest;
import coop.guenoa.afip.wsfev1.FECompConsultaReq;
import coop.guenoa.afip.wsfev1.FECompConsultaResponse;
import coop.guenoa.afip.wsfev1.WsFe_v1;

public class test_wsfev1_ConsultarCae {

	public static void main(String[] args) {

		Debug.debug = true;

		WsFe_v1 wsfev1 = new WsFe_v1(true);

		DummyResponse response = null;
		try {
			response = wsfev1.FEDummy();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("App Server: " + response.getAppServer());
		System.out.println("Auth Server: " + response.getAuthServer());
		System.out.println("Db Server: " + response.getDbServer());

		Configuracion conf = new Configuracion();
		conf.readProperty();

		Wsaa wsaa = new Wsaa(conf);
		wsaa.setService("wsfe");

		Long cuit = Long.valueOf("20286670149");

		FEAuthRequest auth = null;

		try {
			auth = new FEAuthRequest(wsaa.getTicketLogin().getToken(), wsaa.getTicketLogin().getSign(),
					cuit.longValue());
		} catch (WsaaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int ptoVta = 6;
		int cbteTipo = 6;
		long cbteNro = 0;
		try {
			cbteNro = wsfev1.FECompUltimoAutorizado(auth, ptoVta, cbteTipo).getCbteNro();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Puto de Venta: " + ptoVta);
		System.out.println("Comprobante: " + cbteTipo);
		System.out.println("Nro Combrobante: " + cbteNro);

		FECompConsultaReq feCompConsReq = new FECompConsultaReq(cbteTipo, cbteNro, ptoVta);

		FECompConsultaResponse result = null;
		try {
			result = wsfev1.FECompConsultar(auth, feCompConsReq);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Err[] errs = result.getErrors();
		if (errs != null) {
			for (int i = 0; i < errs.length; i++) {
				Err err = errs[i];
				System.err.println("Err " + err.getCode() + err.getMsg());
			}
		}

		Evt[] evs = result.getEvents();
		if (evs != null) {
			for (int i = 0; i < evs.length; i++) {
				Evt evt = evs[i];
				System.out.println("Evs " + evt.getMsg());
			}
		}

	}

}
