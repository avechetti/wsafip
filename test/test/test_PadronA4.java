package test;

import java.rmi.RemoteException;
import java.util.Calendar;

import coop.guenoa.afip.padron.a4.Actividad;
import coop.guenoa.afip.padron.a4.Categoria;
import coop.guenoa.afip.padron.a4.Dependencia;
import coop.guenoa.afip.padron.a4.Domicilio;
import coop.guenoa.afip.padron.a4.Email;
import coop.guenoa.afip.padron.a4.Impuesto;
import coop.guenoa.afip.padron.a4.Persona;
import coop.guenoa.afip.padron.a4.PersonaReturn;
import coop.guenoa.afip.padron.a4.PersonaServiceA4;
import coop.guenoa.afip.util.Configuracion;
import coop.guenoa.afip.util.Debug;
import coop.guenoa.afip.wsaa.TicketLogin;
import coop.guenoa.afip.wsaa.Wsaa;
import coop.guenoa.afip.wsaa.WsaaException;

public class test_PadronA4 {

	public static void main(String[] args) {

		Debug.debug = true;

		PersonaServiceA4 a4 = new PersonaServiceA4();
		
		
		Configuracion conf = new Configuracion();
		conf.readProperty();
		
		Wsaa wsaa = new Wsaa(conf);
		wsaa.setService("ws_sr_padron_a4");
		
		TicketLogin tl = null;
		try {
			tl = wsaa.getTicketLogin();
		} catch (WsaaException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		long idPersona;
		String token;
		long cuitRepresentada;
		String sign;

		token = tl.getToken();
		sign = tl.getSign();
		cuitRepresentada = Long.valueOf("20286670149");
		idPersona = Long.valueOf("30558515305");

		try {
			PersonaReturn personareturn = a4.getPersona(token, sign, cuitRepresentada, idPersona);

			Persona persona = personareturn.getPersona();

			Actividad[] actividad = persona.getActividad();

			for (int i = 0; i < actividad.length; i++) {
				Actividad act = actividad[i];

				System.out.println("Actividad " + act.getIdActividad() + "  " + act.getNomenclador() + " "
						+ act.getDescripcionActividad());

			}

			System.out.println("Apellido: " + persona.getApellido());

			System.out.println("Cantidad de Socios: " + persona.getCantidadSociosEmpresaMono());

			Categoria[] categoria = persona.getCategoria();

			if (categoria != null) {
				for (int i = 0; i < categoria.length; i++) {
					Categoria cat = categoria[i];
					System.out.println("Categoria" + cat.getDescripcionCategoria());

				}
			}

			Long[] claveInactivaAsociada = persona.getClaveInactivaAsociada();
			if (claveInactivaAsociada != null) {
				for (int i = 0; i < claveInactivaAsociada.length; i++) {
					Long long1 = claveInactivaAsociada[i];
					System.out.println("Clave Inactiva Asociada" + long1);
				}
			}

			Dependencia dependencia = persona.getDependencia();
			if (dependencia != null) {
				System.out.println("Dependencia" + dependencia.getDescripcionDependencia());
			}
			;

			Domicilio[] domicilio = persona.getDomicilio();
			if (domicilio != null) {

				for (int i = 0; i < domicilio.length; i++) {
					Domicilio domicilio2 = domicilio[i];
					System.out.println("Domicilio " + domicilio2.getCodPostal());
				}
			}

			Email[] email = persona.getEmail();
			if (email != null) {
				for (int i = 0; i < email.length; i++) {
					Email email2 = email[i];
					System.out.println("Email " + email2.getDireccion());
				}
			}

			String estadoClave = persona.getEstadoClave();

			System.out.println("Estado Clave " + estadoClave.getBytes());

			Calendar fechaContratoSocial = persona.getFechaContratoSocial();

			if (fechaContratoSocial != null) {
				System.out.println("Fecha Contrato Social " + fechaContratoSocial.toString());
			}

			Calendar fechaFallecimiento = persona.getFechaFallecimiento();

			if (fechaFallecimiento != null) {
				System.out.println("Fecha Fallecimiento" + fechaFallecimiento.toString());
			}

			Calendar fechaInscripcion = persona.getFechaInscripcion();
			System.out.println("Fecha Inscripcion " + fechaInscripcion.toString());

			Calendar fechaJubilado = persona.getFechaJubilado();

			if (fechaJubilado != null) {
				System.out.println("Fecha Jubilado " + fechaJubilado.toString());
			}

			/*
			 * Calendar fechaNacimiento = persona.getFechaNacimiento();
			 * 
			 * Calendar fechaVencimientoMigracion = persona.getFechaVencimientoMigracion();
			 * 
			 * String formaJuridica = persona.getFormaJuridica();
			 * 
			 * Long idPersona = persona.getIdPersona();
			 */

			Impuesto[] impuesto = persona.getImpuesto();

			if (impuesto != null) {
				for (int i = 0; i < impuesto.length; i++) {
					Impuesto imp = impuesto[i];
					System.out.println("Impuesto: " + imp.getIdImpuesto() + " " + imp.getDescripcionImpuesto() + " "
							+ imp.getEstado() + " " +  imp.getPeriodo())   ;
				}
			}

			/*
			 * Integer leyJubilacion = persona.getLeyJubilacion();
			 * 
			 * String localidadInscripcion = persona.getLocalidadInscripcion();
			 * 
			 * Integer mesCierre = persona.getMesCierre();
			 * 
			 * String nombre = persona.getNombre();
			 * 
			 * String numeroDocumento = persona.getNumeroDocumento();
			 * 
			 * Long numeroInscripcion = persona.getNumeroInscripcion();
			 * 
			 * String organismoInscripcion = persona.getOrganismoInscripcion();
			 * 
			 * String organismoOriginante = persona.getOrganismoOriginante();
			 * 
			 * Double porcentajeCapitalNacional = persona.getPorcentajeCapitalNacional();
			 * 
			 * String provinciaInscripcion = persona.getProvinciaInscripcion();
			 * 
			 * String razonSocial = persona.getRazonSocial();
			 * 
			 * coop.guenoa.afip.padron.a4.Regimen[] regimen = persona.getRegimen();
			 * 
			 * coop.guenoa.afip.padron.a4.Relacion[] relacion = persona.getRelacion();
			 * 
			 * String sexo = persona.getSexo();
			 * 
			 * coop.guenoa.afip.padron.a4.Telefono[] telefono = persona.getTelefono();
			 * 
			 * String tipoClave = persona.getTipoClave();
			 * 
			 * String tipoDocumento = persona.getTipoDocumento();
			 * 
			 * String tipoOrganismoOriginante = persona.getOrganismoOriginante();
			 * 
			 * String tipoPersona = persona.getTipoPersona();
			 * 
			 * String tipoResidencia = persona.getTipoResidencia();
			 */
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
